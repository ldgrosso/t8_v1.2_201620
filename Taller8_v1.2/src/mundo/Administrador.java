package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.MinPQ;
import estructuras.Nodo;
import estructuras.Queue;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";
	
	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";

	
	private double weight;       // total weight of MST
    private Queue<Arco> mst;     
    private boolean[] marked;    // marked[v] = true if v on tree
    private MinPQ<Arco> pq; 
	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	private static GrafoNoDirigido<String, String> grafoMetro;
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String

	
	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		grafoMetro = new GrafoNoDirigido<>();
		mst = new Queue<Arco>();
		pq= new MinPQ<Arco>();
		
		//TODO inicialice el grafo como un GrafoNoDirigido
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		String[] rta = new String[185];
		int j = 0;
		Arco[] arcosOrigenIdentificador = grafoMetro.darArcosOrigen(identificador);
		if(arcosOrigenIdentificador!=null){
				for (int i = 0; i < arcosOrigenIdentificador.length; i++) {
					if(arcosOrigenIdentificador[i]!=null){
						String ruta = (String)arcosOrigenIdentificador[i].darInformacion();
						if(!estaEnLista(rta, ruta)){
							rta[j] = ruta ;
							j++;
						}
					}
				
			}
		}
		Arco[] arcosFinIdentificador = grafoMetro.darArcosDestino(identificador);
		if(arcosFinIdentificador!=null){
			for (int i = 0; i < arcosFinIdentificador.length; i++) {
				if(arcosFinIdentificador[i]!=null){
					String ruta = (String)arcosFinIdentificador[i].darInformacion();
					if(!estaEnLista(rta, ruta)){
						rta[j] = ruta ;
						j++;
					}
				}
		}
	}
		int k = 0 ;
		int tamanio = 0;
		while(rta[k]!=null)	{
			tamanio++;
			k++;
		}
		String[] rtafinal = new String[tamanio];
		for (int i = 0; i < tamanio; i++) {
			rtafinal[i]= rta[i];
		}
		return rtafinal ;
		//TODO Implementar
	}
	private boolean estaEnLista(String[] lista, String que){
		boolean esta = false;
		for (int i = 0; i < lista.length&&!esta&&lista[i]!=null; i++) {
			if(lista[i].equals(que)) esta = true;
		}
		return esta;
	}
	

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		
		//TODO Implementar
		return 0.0;
	}
	private void LazyPrimMST(GrafoNoDirigido grafo){
		int numNodos = grafoMetro.darNodos().length;
		marked = new boolean[numNodos];
       // for (int v = 0; v < numNodos; v++)     // run Prim from all vertices to
           // if (!marked[v]) prim(G, v);
		
	}
	private void prim (GrafoNoDirigido grafo, int s){
	//	scan(grafo, s);
		
	}
	private void scan(GrafoNoDirigido grafo, String s){
	
	}
	
	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		//TODO Implementar
		return 0.0;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public static void cargarInformacion() throws Exception
	{
		BufferedReader bf = new BufferedReader(new FileReader(RUTA_PARADEROS));
		bf.readLine();
		String linea;
		int i= 0 ;
		while ((linea = bf.readLine())!=null) {
			String[] info = linea.split(";");
			Estacion nueva = new Estacion(info[0],Double.parseDouble(info[1]), Double.parseDouble(info[2]));
			grafoMetro.agregarNodo(nueva);
			i++;
		}
		//TODO Implementar
		
		System.out.println("Se han cargado correctamente "+grafoMetro.darNodos().length+" Estaciones");
		bf.close();
		BufferedReader bf2 = new BufferedReader(new FileReader(RUTA_RUTAS));
		bf2.readLine();
		String linea2 = bf2.readLine();
		boolean continuar = true;
		//while (linea2=="--------------------------------------") {
		while(continuar){
			String nombreRuta = bf2.readLine();
			int limite = Integer.parseInt(bf2.readLine());
			if(limite>0){
				String estacionInicio = bf2.readLine().split(" ")[0];
				if(estacionInicio.equals("380A06 48")) continuar = false;
				int j = 1;
				while(j<limite){
					String[] informacion = bf2.readLine().split(" ");
					grafoMetro.agregarArco(estacionInicio, informacion[0], Double.parseDouble(informacion[1]), nombreRuta);
					j++;
				}
				linea2 = bf2.readLine();
				if(linea2==null){
					continuar = false;
				}
			}
			else{
				linea2 = bf2.readLine();
				if(linea2==null){
					continuar = false;
				}
			}
			
		}
		
		bf2.close();
		//TODO Implementar
		
		System.out.println("Se han cargado correctamente "+ grafoMetro.darArcos().length+" arcos");
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		//TODO Implementar
		return (Estacion<String>) grafoMetro.buscarNodo(identificador);
	}
	public static void main(String[] args ){
		/**try {
			Administrador admi = new Administrador();
			admi.cargarInformacion();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		**/
	}

}
