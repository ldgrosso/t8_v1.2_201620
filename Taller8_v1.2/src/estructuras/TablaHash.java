package estructuras;


public class TablaHash<K extends Comparable<K> ,V> {

	
	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	
	private NodoHash<K,V>[] tablaLP;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;
	

	//Constructores
	public TablaHash( int pCapacidad)
	{
	    factorCargaMax=(float)0.5;
	    capacidad=pCapacidad;
	    tablaLP = new NodoHash[capacidad];
		count=0;
		
	}
	
	public int getCapacidad(){
		return capacidad;
	}
	
	public NodoHash<K,V>[] getTablaLinearP(){
		return tablaLP;
	}

	public void put(K llave, V valor)
	{
		count++;
		
		int a=hash(llave);
		if(tablaLP[a]==null){
			tablaLP[a]= new NodoHash(llave, valor);
		}
		else{
			int i = a;
			while (i < tablaLP.length-1&& tablaLP[i]!=null) {
					i++;
			}
			if(i==tablaLP.length-1 && tablaLP[i]!=null){
				i = 0;
				while (i < tablaLP.length-1&& tablaLP[i]!=null) {
					i++;
				}
			}
			tablaLP[i]=new NodoHash(llave, valor);
			if(llave.equals("201A03")){
			System.out.println(i);
			}
			
		}
		
	}
	
	public V get(K llave)
	{
		V valor= null;
		boolean encontro=false;
		int a=hash(llave);
		int i = a;
		while (i < tablaLP.length&& tablaLP[i]!=null && !encontro) {
			if(tablaLP[i].getLlave().equals(llave)){
				encontro = true;
				valor = tablaLP[i].getValor();
			}
			else{
				i++;
			}
		}
		if(i==tablaLP.length&& tablaLP[i-1]!=null&& !encontro){
			i = 0;
			while (i < tablaLP.length&& tablaLP[i]!=null && !encontro) {
				if(tablaLP[i].getLlave().equals(llave)){
					encontro = true;
					valor = tablaLP[i].getValor();
				}
				else{
					i++;
				}
			}
		}
		return valor;
	}

	public V delete(K llave)
	{
		V valor= null;
		int j = 0;
		boolean encontro=false;
		int a=hash(llave);
		
		
			int i = a;
			while (i < tablaLP.length&& tablaLP[i]!=null && !encontro) {
				if(tablaLP[i].getLlave().equals(llave)){
					encontro = true;
					valor = tablaLP[i].getValor();
				}
				else{
					i++;
				}
			}
			if(i==tablaLP.length-1 && tablaLP[i]!=null&& !encontro){
				i = 0;
				while (i < tablaLP.length&& tablaLP[i]!=null && !encontro) {
					if(tablaLP[i].getLlave().equals(llave)){
						encontro = true;
						valor = tablaLP[i].getValor();
					}
					else{
						i++;
					}
				}
			}
		
		
		return valor;
	}

	//Hash
	private int hash(K llave)
	{
		int hash=llave.hashCode();
		int a = tablaLP.length;
		
		if(hash<=0)
		{
			hash=hash*(-1);
		}
		
		int p=Math.abs((hash%a)-1);
		
		return p;
	}
}
