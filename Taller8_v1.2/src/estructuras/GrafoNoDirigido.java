package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	/**
	 * Nodos del grafo
	 */
	private ArrayList<Nodo<K>> nodos;
	//TODO Declare la estructura que va a contener los nodos

	/**
	 * Lista de adyacencia 
	 */
	private TablaHash<String, TablaHash<String, Arco>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado. 

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		nodos = new ArrayList<>();
		adj = new TablaHash<String, TablaHash<String, Arco>>(5182);
		
		//TODO implementar
	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo) {
		nodos.add(nodo);
		adj.put((String) nodo.darId(), new TablaHash<String,Arco>(1000));
		//TODO implementar
		return true;
	}

	@Override
	public boolean eliminarNodo(K id) {
		boolean encontro = false;
		for (int i = 0; i < nodos.size()&&!encontro; i++) {
			if(nodos.get(i).darId().equals(id)){
				nodos.remove(i);
				encontro = true;
			}
		}
		//TODO implementar
		return encontro;
	}

	@Override
	public Arco<K,E>[] darArcos() {
		
		Arco<K,E>[] rta = new Arco[12520];
		NodoHash<String, TablaHash<String, Arco>>[] externa =adj.getTablaLinearP();
		for (int j = 0; j < nodos.size(); j++) {
			Arco[] subLista = darArcosOrigen(nodos.get(j).darId());
			for (int k = 0; k < subLista.length; k++) {
				rta[k]=subLista[k];
			}
		}
		
		
		return rta;
		//TODO implementar
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		Nodo<K>[] rta = new Nodo[nodos.size()];
		for (int i = 0; i<nodos.size(); i++) {
			rta[i] = nodos.get(i);
		}
		return rta;
		//TODO implementar
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {
		//Crear una tabla con las rutas 
		Arco nuevo = crearArco(inicio, fin, costo, obj);
		TablaHash<String, Arco> subTabla = adj.get((String) inicio);
		subTabla.put((String)fin, nuevo);
		TablaHash<String, Arco> subTabla2 = adj.get((String) fin);
		Arco<String, String> arcoFin = subTabla2.get((String)fin);
		if(arcoFin!=null){
			arcoFin.putTablaFin(nuevo);
		}
		else{
			subTabla2.put((String)fin, nuevo);
		}
		//TODO implementar
		return false;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
		
		TablaHash<String, Arco> subTabla = adj.get((String) inicio);
		subTabla.delete((String)fin);
		
		TablaHash<String, Arco> subTabla2 = adj.get((String) fin);
		//eliminar del hash fin 
		//TODO implementar
		return null;
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		boolean encontro = false;
		
		for (int i = 0; i < nodos.size()&&!encontro; i++) {
			if(nodos.get(i).darId().equals(id)){
				return nodos.get(i);
			}
		}
		//TODO implementar
		return null;
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		int i = 0 ;Arco<K,E>[] rta = null;
		TablaHash<String, Arco> tablaHash = adj.get((String) id);
		if(tablaHash!=null){
			NodoHash<String, Arco>[] subTabla = tablaHash.getTablaLinearP();
			
			if(tablaHash.get((String)id)!=null){
				rta = new Arco[subTabla.length-1];
				for (int j = 0 ; j < subTabla.length; j ++) {
					NodoHash actual = subTabla[j];
					if(actual!=null&&!(actual.getLlave().equals(id))){
						rta[i]= (Arco<K, E>) actual.getValor();
						i++;
					}
				}
				
			}
			else{
				rta = new Arco[subTabla.length];
				for (int j = 0 ; j < subTabla.length; j ++) {
					NodoHash actual = subTabla[j];
					if(actual!=null&&!(actual.getLlave().equals(id))){
						rta[i]= (Arco<K, E>) actual.getValor();
						i++;
					}
				}
			}
		}
		
		return rta;
		//TODO implementar
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		Arco<K,E>[] rta= null;
		int j = 0 ;
		Arco mismo = adj.get((String) id).get((String)id);
		if(mismo!=null){
			
			if(mismo.getTablaFin()!=null){
				NodoHash<String, Arco>[] arrArcosFin = mismo.getTablaFin().getTablaLinearP();
				rta = new Arco[arrArcosFin.length+1];
				for (int i = 1 ; i<arrArcosFin.length; i++) {
					if(arrArcosFin[i]!=null){
						rta[j]= arrArcosFin[i].getValor();
						j++;
					}
					
				}
			}
			rta[0]= mismo;
			
			
			
		}
		
		return rta;
		
		//TODO implementar
	}

}
