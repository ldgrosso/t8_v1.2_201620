package estructuras;

public class NodoHash<K,V> {

	private K llave;
	private V valor;
	private NodoHash<K,V> next; 
	//public NodoHash<K,V> actual;
	
	public NodoHash(K llave, V valor) 
	{
		super();
		this.llave = llave;
		this.valor = valor;
		
	}
	public K getLlave() {
		return llave;
	}
	
	public void setLlave(K llave) {
		this.llave = llave;
	}
	
	public V getValor() 
	{
		return valor;
	}
	
	public void setValor(V valor) {
		this.valor = valor;
	}
	public NodoHash<K,V> getNext() 
	{
		return next;
	}
	public void setNext(NodoHash<K,V> a)
	{
		if(next==null)
		{
			next=a;
		}
		else
		{
			next.setNext(a);
		}
	}
	/**public NodoHash<K,V> setItem()
	{
		return actual;
	}**/
}
