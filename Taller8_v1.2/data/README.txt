The HashMap class is roughly equivalent to Hashtable, 
except that it is unsynchronized and permits nulls.
